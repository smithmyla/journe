import React, { useEffect, useState } from 'react';
import axios from 'axios';
import "./UserProfile.css";

function UserProfile() {
    const [user, setUser] = useState(null);
    const [userID, setUserID] = useState(null)

    const [posts, setPosts] = useState([]);

    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [zodiacSign, setZodiacSign] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    const token = localStorage.getItem("token");


    const getUser = async () => {
        const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`, {
            withCredentials: true,
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })

        const userData = response.data.user;

        setUserID(userData.id);
    }


    const getUserData = async (id) => {

        const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/users/${id}`, {
            withCredentials: true,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })

        const userData = response.data;

        setUser(userData);
    }



    const handleUpdate = async (id) => {

        let body = {
            "first": first,
            "last": last,
            "username": user.username,
            "email": email,
            "password": password,
            "zodiac_sign": zodiacSign
        }

        axios.put(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/users/${user.id}`,
            body, {
            withCredentials: true,
        })
            .then(response => {
                const data = response
                setUser(data.data)

            }
            )
    }


    const handleFirst = (e) => {
        const value = e.target.value;
        setFirst(value);
    };
    const handleLast = (e) => {
        const value = e.target.value;
        setLast(value);
    };
    const handleEmail = (e) => {
        const value = e.target.value;
        setEmail(value);
    };
    const handlePassword = (e) => {
        const value = e.target.value;
        setPassword(value);
    };
    const handleZodiacSign = (e) => {
        const value = e.target.value;
        setZodiacSign(value);
    };
    const handleUsername = (e) => {
        const value = e.target.value;
        setUsername(value);
        console.log(username)
    };

    const getPosts = async (id) => {
        // eslint-disable-next-line
        const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/specificfeeds/${id}`,
            {
                withCredentials: true
            })
            .then(response => {
                const data = response
                console.log(data)
                setPosts(data.data)
            }
            )

    };


    useEffect(() => {
        getUser();
    });

    useEffect(() => {
        if (userID != null) {
            getUserData(userID);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userID])

    useEffect(() => {
        if (user != null) {
            getPosts(userID);
            setIsLoading(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user])
    useEffect(() => {
        if (user !== null && posts !== []) {
            setIsLoading(false)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [posts])


    return (

        < div className="container rounded bg-white mt-5 mb-5" >
            {isLoading && (<div>Loading data!</div>)}
            {!isLoading && (
                <div className="row" key={user.id}>
                    <div className="col-md-3 border-right">
                        <div className="d-flex flex-column align-items-center text-center p-3 py-5">
                            <img className="rounded-circle mt-5" width="150px" src="https://media.istockphoto.com/id/1195719672/vector/portrait-of-a-strong-beautiful-woman-in-profile-with-brown-hair.jpg?s=170667a&w=0&k=20&c=SXeqrXx7pP_QPnisukuxhr5D7grzHOLh0S8iYLDFkqc=" alt="profile"></img>
                            <br></br>
                            <span className="font-weight-bold">{user.first} {user.last}</span>
                            <br></br>
                            <span className="text-black-50">Username: {user.username}</span>
                            <br></br>
                            <span className="text-black-50">Email: {user.email}</span>
                            <br></br>
                            {user.zodiac_sign && (<span className="text-black-50">{user.zodiac_sign}</span>)}
                            {!user.zodiac_sign && (<span className="text-black-50">Anti-Zodiac like Brad</span>)}
                            <span> </span>
                        </div>
                    </div>
                    <div className="col-md-5 border-right">
                        <div className="p-3 py-5">
                            <div className="d-flex justify-content-between align-items-center mb-3">
                                <h4 className="text-right">Update Profile</h4>
                            </div>
                            <div className="row mt-3">
                                <div className="col-md-12">
                                    <label className="labels">Username</label>
                                    <input onChange={handleUsername} type="text" className="form-control" name="username" id="username" placeholder="username" value={user.username} readOnly />
                                </div>
                            </div>
                            <div className="row mt-2">
                                <div className="col-md-6">
                                    <label className="labels">First Name</label>
                                    <input onChange={handleFirst} required type="text" className="form-control" name="first" id="first" placeholder="first name" value={first} />
                                </div>
                                <div className="col-md-6">
                                    <label className="labels">Last Name</label>
                                    <input onChange={handleLast} required type="text" className="form-control" name="last" id="last" placeholder="last name" value={last} />
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col-md-6">
                                    <label className="labels">Email</label>
                                    <input onChange={handleEmail} required type="text" className="form-control" name="email" id="email" placeholder="email" value={email} />
                                </div>
                                <div className="col-md-6">
                                    <label className="labels">Password</label>
                                    <input onChange={handlePassword} type="password" className="form-control" name="password" id="password" placeholder="password" value={password} />
                                </div>
                            </div>
                            <div className="row mt-3">
                                <div className="col-md-6">
                                    <label className="labels">Zodiac Sign</label>
                                    <select
                                        onChange={handleZodiacSign}
                                        value={zodiacSign}
                                        name="zodiac_sign"
                                        id="zodiac_sign"
                                        className="form-select"
                                        placeholder="zodiac_sign"
                                    >
                                        <option value="">Zodiac Sign</option>
                                        <option value="Aries">Aries</option>
                                        <option value="Taurus">Taurus</option>
                                        <option value="Gemini">Gemini</option>
                                        <option value="Cancer">Cancer</option>
                                        <option value="Leo">Leo</option>
                                        <option value="Virgo">Virgo</option>
                                        <option value="Libra">Libra</option>
                                        <option value="Scorpio">Scorpio</option>
                                        <option value="Sagittarius">Sagittarius</option>
                                        <option value="Capricorn">Capricorn</option>
                                        <option value="Aquarius">Aquarius</option>
                                        <option value="Pisces">Pisces</option>
                                    </select>
                                </div>
                            </div>
                            <div className="mt-5 text-center">
                                <button className="btn btn-primary profile-button" onClick={() => handleUpdate(user.id)}>Save Profile</button>
                            </div>
                        </div>
                    </div>
                    <div className="p-3 py-5">
                        <div className="d-flex justify-content-between align-items-center mb-3">
                            <h4 className="text-right">My Posts</h4>
                        </div>
                        <section className="post-container">
                            {posts && (
                                <div>
                                    {posts.map((post) => (
                                        <div key={post.id} className="col-md-4">
                                            <div className="post-card">
                                                <div className="post-card__header">
                                                    <img
                                                        src={post.picture_url}
                                                        alt="card__image"
                                                        className="post-card__image"
                                                        width="600"
                                                    />
                                                </div>
                                                <div className="post-card__body">
                                                    <h4>{post.subject}</h4>
                                                    <h6>{post.post_date}</h6>
                                                    <h6>{post.category}</h6>
                                                    <p>{post.description}</p>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            )}
                        </section>
                    </div>
                </div>
            )}
        </div >

    );
}

export default UserProfile;
