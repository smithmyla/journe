import React, { useState, useEffect, useCallback } from "react";
import { useLocation } from "react-router-dom";
import axios from "axios";


export default function FeedsForm() {
  const [accessUrl, setAccessUrl] = useState("");
  const [fileName, setFileName] = useState("");
  const [fileSentAws, setFileSentAws] = useState(false);

  const location = useLocation();
  const params = location.state;

  // eslint-disable-next-line
  const [userID, setUserID] = useState(localStorage.getItem("userID"));

  useEffect(() => {
    const getUserID = async () => {
      try {
        const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`, {
          withCredentials: true,
        });
        const { access_token, user } = response.data;
        const userID = user.id;
        if (userID) {
          setUserID(userID);

          localStorage.setItem("userID", userID);
        }

        localStorage.setItem("token", access_token);
      } catch (error) {
        console.log("Error occurred while fetching user ID:", error);
      }
    };

    getUserID();
  }, []);


  const [formData, setFormData] = useState({
    user_id: localStorage.userID,
    subject: "",
    favorite: false,
    latitude: "",
    longitude: "",
    post_date: "",
    category: "",
    picture_url: "",
    description: "",
  });


  useEffect(() => {
    if (params) {
      setFormData((prevFormData) => ({
        ...prevFormData,
        longitude: params.northEast,
        latitude: params.southWest,
      }));
    }
  }, [params]);

  const handleFormChange = (e) => {
    const { name, value, type, checked } = e.target;
    const finalValue = type === "checkbox" ? checked : value === "on" ? true : value;

    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: finalValue,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/s3Url`, {
        headers: { "x-amz-acl": "public-read" },
      });

      setAccessUrl(response.data);
      const fileName = response.data.split("/").pop().split("?")[0];
      setFileName(fileName);
    } catch (error) {
      console.error("Error retrieving access URL:", error);
    }
  };

  const sendPost = useCallback(async () => {
    const postUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/feeds`;

    try {
      const response = await fetch(postUrl, {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        setFormData({
          user_id: "",
          subject: "",
          favorite: false,
          longitude: "",
          latitude: "",
          post_date: "",
          category: "",
          picture_url: "",
          description: "",
        });
      } else {
        console.error("Error sending post:", response.status);
      }
    } catch (error) {
      console.error("Error sending post:", error);
    }
  }, [formData]);

  useEffect(() => {
    if (accessUrl !== "") {
      const imageInput = document.querySelector("#imageInput");
      const file = imageInput.files[0];

      const sendRequest = async () => {
        try {
          const response = await fetch(accessUrl, {
            method: "PUT",
            body: file,
          });

          if (response.ok) {
            setFormData((prevFormData) => ({
              ...prevFormData,
              picture_url: `https://devbucketjourne.s3.amazonaws.com/${fileName}`,
            }));
            setFileSentAws(true);
            setAccessUrl("");
          } else {
            console.error("Error uploading file:", response.status);
          }
        } catch (error) {
          console.error("Error uploading file:", error);
        }
      };

      sendRequest();
    }
  }, [accessUrl, fileName]);

  useEffect(() => {
    if (fileSentAws) {
      sendPost();
      setFileSentAws(false);
    }
  },
    [fileSentAws, sendPost]);

  return (
    <div className="row">
      <div className="offset-3 col-6 mb-5 mt-5">
        <div className="shadow p-4 mt-3" style={{ backgroundColor: "rgba(255, 255, 255, 0.8)", borderRadius: "10px" }}>
          <h1 className="text-center">Add Post</h1>
          <form onSubmit={handleSubmit} id="add-post">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.user_id}
                placeholder="user_id"
                required
                type="number"
                name="user_id"
                id="user_id"
                className="form-control"
                disabled={true}
              />
              <label htmlFor="user_id">User ID</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.subject}
                placeholder="subject"
                required
                type="text"
                name="subject"
                id="subject"
                className="form-control"
              />
              <label htmlFor="subject">Subject</label>
            </div>
            <div className="form-check mb-3">
              <input
                onChange={handleFormChange}
                checked={formData.favorite}
                type="checkbox"
                name="favorite"
                id="favorite"
                className="form-check-input"
              />
              <label htmlFor="favorite" className="form-check-label">
                Favorite
              </label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.longitude}
                placeholder="longitude"
                required
                type="number"
                name="longitude"
                id="longitude"
                className="form-control"
                disabled={params}
              />
              <label htmlFor="longitude">Longitude</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.latitude}
                placeholder="latitude"
                required
                type="number"
                name="latitude"
                id="latitude"
                className="form-control"
                disabled={params}

              />
              <label htmlFor="latitude">Latitude</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.post_date}
                placeholder="post_date"
                required
                type="date"
                name="post_date"
                id="post_date"
                className="form-control"
              />
              <label htmlFor="post_date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.category}
                required
                name="category"
                id="category"
                className="form-select"
              >
                <option value="">Select Category</option>
                <option value="POI">POI</option>
                <option value="Event">Event</option>
                <option value="Other">Other</option>
              </select>
              <label htmlFor="category">Category</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="picture_url"
                required
                type="file"
                accept="image/*"
                name="picture_url"
                id="imageInput"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture upload</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={handleFormChange}
                value={formData.description}
                placeholder="description"
                required
                type="text"
                name="description"
                id="description"
                className="form-control"
              />
              <label htmlFor="description">Description</label>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
