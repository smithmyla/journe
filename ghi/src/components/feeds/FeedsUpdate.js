import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

export default function FeedsForm() {
  const { id } = useParams();
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    user_id: "",
    subject: "",
    favorite: false,
    longitude: "",
    latitude: "",
    post_date: "",
    category: "",
    picture_url: "",
    description: "",
  });

  useEffect(() => {
    const fetchFeed = async () => {
      const fetchUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/feeds/${id}`;
      const response = await fetch(fetchUrl);
      if (response.ok) {
        const feedData = await response.json();
        setFormData(feedData);
      } else {
        console.error("Error fetching feed data");
      }
    };

    fetchFeed();
  }, [id]);

  const handleFormChange = (e) => {
    const { name, value, type, checked } = e.target;
    const finalValue = type === "checkbox" ? checked : value === "on" ? true : value;

    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: finalValue,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const updateUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/feeds/${id}`;
    console.log(updateUrl);

    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(updateUrl, fetchConfig);
    
    if (response.ok) {
      navigate(`/feeds`);
    } else {
      console.error("Error updating feed");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6 mb-5 mt-5">
        <div className="shadow p-4 mt-3">
          <h1 className="text-center">Update Post</h1>
          <form onSubmit={handleSubmit} id="update-post">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.user_id}
                placeholder="user_id"
                required
                type="number"
                name="user_id"
                id="user_id"
                className="form-control"
              />
              <label htmlFor="user_id">User ID</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.subject}
                placeholder="subject"
                required
                type="text"
                name="subject"
                id="subject"
                className="form-control"
              />
              <label htmlFor="subject">Subject</label>
            </div>
            <div className="form-check mb-3">
              <input
                onChange={handleFormChange}
                checked={formData.favorite}
                type="checkbox"
                name="favorite"
                id="favorite"
                className="form-check-input"
              />
              <label htmlFor="favorite" className="form-check-label">
                Favorite
              </label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.longitude}
                placeholder="longitude"
                required
                type="number"
                name="longitude"
                id="longitude"
                className="form-control"
              />
              <label htmlFor="longitude">Longitude</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.latitude}
                placeholder="latitude"
                required
                type="number"
                name="latitude"
                id="latitude"
                className="form-control"
              />
              <label htmlFor="latitude">Latitude</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.post_date}
                placeholder="post_date"
                required
                type="date"
                name="post_date"
                id="post_date"
                className="form-control"
              />
              <label htmlFor="post_date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleFormChange}
                value={formData.category}
                required
                name="category"
                id="category"
                className="form-select"
              >
                <option value="">Select Category</option>
                <option value="POI">POI</option>
                <option value="Event">Event</option>
                <option value="Other">Other</option>
              </select>
              <label htmlFor="category">Category</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.picture_url}
                placeholder="picture_url"
                required
                type="url"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="form-floating mb-3">
              <textarea
                onChange={handleFormChange}
                value={formData.description}
                placeholder="description"
                required
                type="text"
                name="description"
                id="description"
                className="form-control"
              />
              <label htmlFor="description">Description</label>
            </div>
            <button type="submit" className="btn btn-primary">
              Update
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
