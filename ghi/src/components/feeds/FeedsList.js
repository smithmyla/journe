import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./posts.css";

const token = localStorage.getItem("token");
console.log(localStorage);
export default function FeedsList() {

  // eslint-disable-next-line
  const [userID, setUserID] = useState(null);
  const [posts, setPosts] = useState([]);
  // eslint-disable-next-line
  const [isLoading, setIsLoading] = useState(true);



  useEffect(() => {
    const getUserID = async () => {
      try {
        const response = await axios.get(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`, {
          withCredentials: true,
        });
        const { access_token, user } = response.data;
        const userID = user.id;
        if (userID) {
          setUserID(userID);

          localStorage.setItem("userID", userID);
        }
        localStorage.setItem("token", access_token);
      } catch (error) {
        console.log("Error occurred while fetching user ID:", error);
      }
    };

    getUserID();
  }, []);


  const getPosts = async () => {
    try {
      const userID = localStorage.getItem("userID");
      if (!userID) {
        setIsLoading(false);
        return;
      }

      const response = await axios.get(
        `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/specificfeeds/${userID}`,
        {
          withCredentials: true,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (response.status === 200) {
        const data = response.data;

        if (Array.isArray(data)) {
          setPosts(data);
        } else {
          console.log("Invalid feeds data:", data);
        }
      } else {
        console.log("Failed to fetch feeds");
      }

    } catch (error) {
      console.log("Error occurred while fetching feeds:", error);
    } finally {
      setIsLoading(false);
    }
  };


  useEffect(() => {
    getPosts();
  }, [userID])

  const deleteFeed = async (id) => {
    const deleteUrl = `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/feeds/${id}`;
    const response = await fetch(deleteUrl, {
      method: "DELETE",
    });
    console.log(response);
    if (response.ok) {
      setPosts(posts.filter((f) => f.id !== id));
    }
  };

  const nav = useNavigate();

  const handleUpdateClick = (id) => {
    nav(`update/${id}`);
  };

  return (
    <>
      <section className="hero-section">
        <div className="card-grid">
          {posts.map((post) => (
            <div className="card" key={post.id}>
              <div
                className="card__background"
                style={{ backgroundImage: `url(${post.picture_url})` }}
              ></div>
              <div className="card__content">
                <p className="text-with-shadow">{post.category}</p>
                <h3 className="text-with-shadow">{post.subject}</h3>
                <h5 className="text-with-shadow">{post.description}</h5>
              </div>
              <button
                className="button-24"
                onClick={() => handleUpdateClick(post.id)}
              >
                Update
              </button>
              <button
                className="button-25"
                onClick={() => deleteFeed(post.id)}
              >
                Delete
              </button>
            </div>
          ))}
        </div>
      </section>
    </>
  );
}
