import { NavLink } from "react-router-dom";
import { setAuthToken } from './components/auth/AuthToken';
import "./Nav.css";

const token = localStorage.getItem("token");

function Nav() {


  function logoutClick(e) {
    e.preventDefault();
    localStorage.removeItem("token")
    setAuthToken();
    window.location.reload();

  }

  return (
    <>
      <nav className="navbar">
        <div id="trapezoid">
          <NavLink className="expandHome navbar-link" to="/">
            JournE
          </NavLink>
          <div className="subnav">
            <button className="subnavbtn">
              Accounts<i className="fa fa-caret-down"></i>
            </button>
            <div className="subnav-content">
              <div id="subnav-trapezoid">
                <NavLink to="/users">Account List</NavLink>
                <NavLink to="/profile">Profile</NavLink>
              </div>
            </div>
          </div>
          <div className="subnav">
            <button className="subnavbtn">
              Posts<i className="fa fa-caret-down navbar-link"></i>
            </button>
            <div className="subnav-content">
              <div className="subnav-trapezoid">
                <NavLink href="#" to="/feeds">
                  My Posts
                </NavLink>
                <NavLink href="#" to="/feeds/all">
                  All Posts
                </NavLink>
                <NavLink href="#" to="/feeds/new">
                  Create a Post
                </NavLink>
              </div>
            </div>
          </div>
          <NavLink href="#" to="/aboutus" className="expandHome navbar-link">
            About Us
          </NavLink>
          {!token && (
            <NavLink href="#" to="/users/signup" className="expandHome navbar-link">
              Sign Up
            </NavLink>
          )}
          {!token && (
            <NavLink href="#" to="/login" className="expandHome navbar-link">
              Login
            </NavLink>
          )}
          {token && (
            <>
              <button className="logout-button navbar-link" onClick={logoutClick}>
                Logout
              </button>
            </>
          )}
        </div>
      </nav>
    </>
  );
}

export default Nav;
