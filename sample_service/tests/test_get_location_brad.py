from fastapi.testclient import TestClient
from queries.feeds import FeedQueries
from main import app
from routers.feeds import FeedsOut

client = TestClient(app)


class FeedQueriesMock:
    def get_feeds_by_coordinates(self, from_lat, to_lat, from_lng, to_lng):
        return [

            {
                "id": 1,
                "user_id": 2,
                "subject": "Cool Place",
                "favorite": True,
                "longitude": -85,
                "latitude": 33,
                "post_date": "2023-06-09",
                "category": "Event",
                "picture_url": "picture.jpg",
                "description": "A cool place to chill."
            }
        ]


def test_get_location():

    app.dependency_overrides[FeedQueries] = FeedQueriesMock

    response = client.get(
        "/api/locations", params={"from_lat": 25, "to_lat": 36.0, "from_lng": -80.0, "to_lng": -90.0})

    expected_response = {"feeds": [{
        "id": 1,
        "user_id": 2,
        "subject": "Cool Place",
        "favorite": True,
        "longitude": -85,
        "latitude": 33,
        "post_date": "2023-06-09",
        "category": "Event",
        "picture_url": "picture.jpg",
        "description": "A cool place to chill."
    }]}

    assert response.status_code == 200
    assert response.json() == expected_response

    app.dependency_overrides = {}
