from queries.users import UserQueries
from fastapi.testclient import TestClient
from main import app
from pydantic import BaseModel

client = TestClient(app)


class UserOut(BaseModel):
    id: int
    first: str
    last: str
    username: str
    email: str
    pasword: str
    zodiac_sogn: str


class UserQueriesMock:
    def get_all_users(self):
        return [
            {
                "id": 1,
                "first": "ff",
                "last": "ss",
                "username": "ff",
                "email": "ff@aol.com",
                "password": "password",
                "zodiac_sign": "Aquarius",
            },
        ]


def test_get_all_users():
    app.dependency_overrides[UserQueries] = UserQueriesMock

    response = client.get("api/users")

    assert response.status_code == 401
    assert len(response.json()) >= 1

    app.dependency_overrides = {}