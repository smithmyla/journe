from queries.pool import pool


class FeedQueries:
    def get_all_feeds(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, user_id, subject,
                    favorite, longitude, latitude, post_date, category, picture_url,
                    description

                    FROM feeds
                    order by post_date desc
                    """
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_feeds_by_userid(self, user_id):

        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """

                    SELECT * from feeds
                    WHERE user_id = %s
                    ORDER BY post_date DESC;
                    """,
                    (user_id,),
                )
                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results if results else None

    def get_feeds_by_coordinates(self, from_lat, to_lat, from_lng, to_lng):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, user_id, subject,
                    favorite, longitude, latitude, post_date, category, picture_url,
                    description

                    FROM feeds
                    WHERE latitude > %s
                    AND latitude < %s
                    AND longitude > %s
                    AND longitude < %s
                    ORDER BY post_date DESC;
                    """,
                    (from_lat, to_lat, from_lng, to_lng),
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)
                print(results)
                return results

    def get_feed(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT * from feeds
                    WHERE id = %s
                """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def create_feed(self, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.user_id,
                    data.subject,
                    data.favorite,
                    data.longitude,
                    data.latitude,
                    data.post_date,
                    data.category,
                    data.picture_url,
                    data.description,
                ]
                cur.execute(
                    """
                    INSERT INTO feeds (user_id, subject, favorite, longitude, latitude, post_date, category, picture_url, description)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, user_id, subject, favorite, longitude, latitude, post_date, category, picture_url, description
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def update_feed(self, feed_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.user_id,
                    data.subject,
                    data.favorite,
                    data.longitude,
                    data.latitude,
                    data.post_date,
                    data.category,
                    data.picture_url,
                    data.description,
                    feed_id,
                ]
                cur.execute(
                    """
                    UPDATE feeds
                    SET user_id = %s
                      , subject = %s
                      , favorite = %s
                      , longitude = %s
                      , latitude = %s
                      , post_date = %s
                      , category = %s
                      , picture_url = %s
                      , description = %s
                    WHERE id = %s
                    RETURNING id, user_id, subject, favorite, longitude, latitude, post_date, category, picture_url, description
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def delete_feed(self, feed_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM feeds
                    WHERE id = %s
                    """,
                    [feed_id],
                )
