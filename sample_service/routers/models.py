from pydantic import BaseModel
from typing import Optional
from jwtdown_fastapi.authentication import Token


class UserIn(BaseModel):
    first: str
    last: str
    email: str
    password: str
    username: str
    zodiac_sign: Optional[str]
    id: Optional[int]


class UserOut(UserIn):
    id: int


class UsersOut(BaseModel):
    users: list[UserOut]


class DuplicateUserError(ValueError):
    pass


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


class UserOutWithPassword(UserOut):
    hashed_password: str
