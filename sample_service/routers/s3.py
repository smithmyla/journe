import os
import boto3
import binascii
from random import randbytes
from botocore.config import Config
import time

access_key_id = "AKIAX36PMDYAOABILOEG"
secret_access_key = "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"

s3_client = boto3.client(
    "s3",
    aws_access_key_id=access_key_id,
    aws_secret_access_key=secret_access_key,
    region_name="us-east-2",
    config=Config(signature_version="s3v4"),
)

bucket_name = "devbucketjourne"


def generate_upload_url():
    timestamp = str(int(time.time() * 1000))
    raw_bytes = os.urandom(16)
    image_name = timestamp + "_" + binascii.hexlify(raw_bytes).decode("utf-8")
    response = s3_client.generate_presigned_url(
        "put_object",
        Params={"Bucket": "devbucketjourne", "Key": image_name},
        ExpiresIn=60,
    )

    return response
